//
//  CafeViewController.swift
//  Cafe
//
//  Created by roman on 7/25/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class CafeViewController: UIViewController {

    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var numberOfGuestsTextField: UITextField!
    @IBOutlet weak var numberOfSeatTextField: UITextField!
    
    @IBOutlet weak var bookedSwitch: UISwitch!
    @IBOutlet weak var prepaymentSwitch: UISwitch!
    @IBOutlet weak var vipRoomSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func billAlertButtonTapped(_ sender: UIButton) {
        self.alertBillMenu(title: "Bill", message: "Do you want check?", style: .alert)
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func alertBillMenu(title: String, message: String, style: UIAlertController.Style) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let cancelButton = UIAlertAction(title: "cancel", style: .default) { (action) in
            
        }
        // MARK - data transfer to CheckVC
        let CheckButton = UIAlertAction(title: "check", style: .default) { (action) in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let checkVC = storyBoard.instantiateViewController(withIdentifier: "check") as! CheckViewController
            
            
            checkVC.nameText = self.fullNameTextField.text ?? ""
            checkVC.numberOfSeatText =  self.numberOfGuestsTextField.text ?? ""
            checkVC.sumText = "200 dollars"

            checkVC.booked = self.offOnSwitch(switchPosition: self.bookedSwitch.isOn)
            checkVC.prepayment = self.offOnSwitch(switchPosition: self.prepaymentSwitch.isOn)
            checkVC.vipRoom = self.offOnSwitch(switchPosition: self.vipRoomSwitch.isOn)
            
            self.show(checkVC, sender: nil)
        }
        
        alertController.addAction(cancelButton)
        alertController.addAction(CheckButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func offOnSwitch(switchPosition: Bool) -> String {
        if switchPosition == true {
            return "yes"
        } else {
            return "no"
        }
    }

}
