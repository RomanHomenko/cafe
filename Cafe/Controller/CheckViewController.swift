//
//  CheckViewController.swift
//  Cafe
//
//  Created by roman on 7/25/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class CheckViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberOfSeatLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    
    @IBOutlet weak var bookedLabel: UILabel!
    @IBOutlet weak var prepaymentLabel: UILabel!
    @IBOutlet weak var vipRoomLabel: UILabel!
    
    var nameText: String = ""
    var numberOfSeatText: String = ""
    var sumText: String = ""
    var booked: String = ""
    var prepayment: String = ""
    var vipRoom: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = nameText
        numberOfSeatLabel.text = numberOfSeatText
        sumLabel.text = sumText
        bookedLabel.text = booked
        prepaymentLabel.text = prepayment
        vipRoomLabel.text = vipRoom
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    

}
